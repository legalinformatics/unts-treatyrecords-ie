# Turning UNTS treaty records into useable data

This project develops a tool for converting data contained in UNTS treaty records into a semantic web format. It does this by annotating UNTS treaty pages gathered with the [UNTS crawler](https://gitlab.com/legalinformatics/unts-crawler) and populating the [PILO ontology](https://gitlab.com/legalinformatics/pilo) as part of a [GATE](https://gate.ac.uk) text processing pipeline. The resulting database can be searched and analyzed in GATE, [Protégé](https://protege.stanford.edu/) or other ontology/triplestore providers, and specific datasets can be extracted in JSON or CSV format with a suitable SPARQL query.

GATE is a robust, longstanding, open source software toolkit designed to be useful for programmers and non-programmers alike, in research or applied contexts, and hence seemed like a good platform to build on. Still, the core information extraction (IE) component of the custom pipeline uses Java regular expressions, which are very similar to regular expressions in other programming languages, and thus the pipeline could be reproduced in another language/framework without too much effort if a better option were to emerge.

The contents of the repository are:
- `img`: screenshots of the applications
- `parsers`:
  - `UNTSregexps.jape`: A regex pattern file to be used with the Java Regexp Annotator plugin as a processing resource in a corpus pipeline
  - `UNTSontopop.jape`: A grammar file to be used with an ontology-aware JAPE Plus Transducer as a processing resource in the same corpus pipeline
- `examples`:
  - `scraped-records`: a set of sample UNTS treaty records exactly as saved by the UNTS crawler, to serve as input to the GATE pipeline
  - `GATE-UNTSrecordsDB`: An Apache Lucene-based searchable datastore (GATE's ANNIC) with the annotated sample treaty records
  - `GATE-UNTSrecordsDB-index`: Datastore index
  - `urlMap.tsv`: A tab-separated file with URLs in the first column and their corresponding treaty identifier in the second column, for linking information from different sources to the same treaty in the database
  - `pilo_sampleUNTSdata.ttl`: A TURTLE file containing the PILO ontology populated with UNTS data of sample treaties
  - `UNTSmetaSamples.csv` : Output of UNTSmeta SPARQL query (with Apache Jena ARQ command line tool)
  - `UNTSparticPOPconv.csv`: Output of UNTSparticPOPconv SPARQL query
- `sparql`:
  - `UNTSmeta.rq`: A sample SPARQL query to extract metadata (without participation data) on all treaties
  - `UNTSparticPOPconv.rq`: A sample SPARQL query to retrieve participation data for the Convention on Persistent Organic Pollutants
- `UNTSrecordsIE.xgapp`: A GATE application file with all settings to reproduce the application, but not including external resources
- `UNTSrecordsIE.zip`: A GATE application bundle including external resources

A brief introduction can be found hereafter under the following headings:

- [About the data](#about-the-data)
- [Related projects](#related-projects)
- [Installation instructions](#installation-instructions)
- [How it works](#how-it-works)

More background and details will be described at https://martinakunz.gitlab.io/treaty-analytics/

## About the data
The United Nations Treaty Series [(UNTS)](https://treaties.un.org/Pages/Content.aspx?path=DB/UNTS/pageIntro_en.xml) is by far the largest and most authoritative collection of international agreements of the UN era. Under Article 102 of the [UN Charter](https://www.un.org/en/about-us/un-charter/full-text), all international agreements concluded by its member states must be registered with the UN Secretariat and be published by it. This publication used to be in paper format only, but today there are two electronic formats: UNTS volumes in PDF, and UNTS online treaty records. While it is the latter that this project focuses on, information on the former is captured for future use.

Unfortunately, the UN treaty section is underfunded and data updates are not automated, even between different UN agencies. Thus, participation data of multilateral agreements _not_ deposited with the UN Secretary-General is often out of date by many years (impression based on anecdotal evidence, not a comprehensive review). The tools and standards chosen for the overarching project are able to handle integration and prioritization of data from different sources (and in different languages).


## Related projects
- [UNTS crawler](https://gitlab.com/legalinformatics/unts-crawler) : Takes a set of UNTS treaty record URLs and saves the information contained in treaty pages as TXT files, adding source and retrieval date information, for processing by GATE
- [PILO](https://gitlab.com/legalinformatics/pilo) : Public International Law Ontology in OWL2 format developed for the purpose of data integration
- [Treaty texts IE](https://gitlab.com/legalinformatics/treatytexts-ie) : GATE application for annotating treaty texts and populating PILO with relevant data (combined with UNTS treaty records data where available)
- [Treaty references](https://gitlab.com/martinakunz/treaty-references) : Uses UNTS metadata retrieved from PILO for generating a bibliographic database and references
- [Treaty participation maps](https://gitlab.com/legalinformatics/treaty-participation-maps): Uses UNTS participation data retrieved from PILO for interactive treaty participation maps


## Installation instructions

Prerequisites:
1. Check whether a suitable Java version is already installed (ideally the 64bit version of Java 11 or later)[^1]
   -> enter "java -version" on a Linux, Mac, or Windows terminal/command prompt 
2. Download and install GATE Developer 9.0.1 from https://gate.ac.uk/download/ (choose installer based on your operating system)
   -> (on Linux) run installer with $java -jar gate-developer-9.0.1-installer.jar
3. Start GATE Developer

Open the application using the provided ZIP archive:
1. Download and extract `UNTSrecordsIE.zip` to a suitable location
2. In GATE Developer, click on "File" -> "Restore Application from File...", navigate to the extracted folder and select `application.xgapp`
3. Run application and check results

**Note**: For full support of OWL2 representation and logical inference, I'd recommend installing Protégé Desktop from https://protege.stanford.edu/products.php -- GATE ontology support is limited (see [Chapter 14](https://gate.ac.uk/sale/tao/splitch14.html#x19-35500014) of the User Guide). 

Reproducing the application from scratch (for the experienced or adventurous):
1. Clone this git repository or download its contents as an archive and unpack it
   - The repo should ideally be downloaded into a folder named 'git' in the user's home directory ($HOME) alongside [PILO](https://gitlab.com/legalinformatics/pilo) for the file paths to work out of the box, otherwise the file path for urlMap.tsv in UNTSontopop.jape needs to be changed (which can be done in any text editor)
2. Install GATE's Ontology Plugin (see GATE User Guide section 14.3)
3. In GATE's CREOLE plugin manager, tick 'Load Now' and 'Load Always' for the Ontology plugin you just installed, as well as for the Ontology Tools, JAPE Plus, String Annotation, and Information Retrieval plugins
4. Under Language Resources, create a new empty GATE Corpus (e.g. called 'UNTSrecords-samples'), then right click and choose 'Populate', navigating to the 'scraped-records' folder of the 'examples' directory (file encoding is UTF-8) -- once created double click on the corpus to check whether the documents are correctly added
5. Still under Language Resources, instantiate a new OWLIM Ontology called 'PILO' with Value fields empty except for:
   - loadImports: false[^2]
   - persistent: false
   - turtleURL: path to pilo/core/pilo.ttl
6. Under Processing Resources, right click -> 'New' -> 'Java Regexp Annotator', enter an informative name for the resource, e.g. 'Java Regexp Annotator UNTSrecords' and navigate to the UNTSregexps.jape file (in the 'parsers' folder) for the patternFileURL parameter, then click OK
7. Likewise, instantiate a new JAPE-Plus Transducer with default settings and the file path of 'UNTSontopop.jape' as the grammarURL of the processing resource
8. Generate a new Document Reset PR in the same vein
9. Under Applications, choose 'Create New Application' -> 'Conditional Corpus Pipeline' and give it a name like 'UNTStreatyrecordsIE' (to distinguish it from the treatytextIE app if you use that too)
10. Double click on the new application and add the three processing resources as follows:
   1. Document Reset PR
      - Corpus: select UNTSrecords-samples from dropdown menu
      - List Type parameters all empty '[]'
      - keepOriginalMarkupAS set to false
   2. Java Regexp Annotator UNTSrecords with default settings except for:
      - matchPreference: ALL
      - outputAnnotationSet: Type
      - overlappingMatches: true
   3. JAPE-Plus Transducer UNTSontopop with default settings except for:
      - inputASName: Type
      - ontology: PILO
      - outputASName: Type
11. Run the application and check the Messages tab for errors/warnings (anything in red font) -- it should take less than a minute to run
    - Warnings that an instance already exists are nothing to worry about
    - Entries about Java EDT violations quickly fill up the log but don't affect the result of the application (I'm working on getting rid of them, help is welcome)
12. (Optional) create new Lucene Based Searchable Datastore and save the new corpus to it for searching and viewing annotations (see GATE User Guide Chapter 9), with the following parameters:
    1. Database directory is GATE-UNTSrecordsDB, and index is GATE-UNTSrecordsDB-index (in the 'examples' folder)
    2. Annotation Sets include field is set to empty (all annotation sets found in the documents)
    3. Base Token Type: Token, with box 'Create Tokens Automatically' ticked
    4. Index Unit Type: leave empty
    5. Features: leave default setting (exclude SpaceToken;Split)
13. Check out corpus statistics and annotations in the Lucene Datastore Searcher and/or annotations in individual treaty records
14. Right click on PILO (listed under Language Resources), choose 'Save as...', provide a target directory and file name, e.g. examples/pilo_populated.ttl, change format for Annotation Sets from rdfxml to turtle, and click OK
15. Compare whether the content of the existing pilo_sampleUNTSdata.ttl file is identical to your output file (exact number of bytes, or comparing file differences with a text editor)

## How it works

Running the application is very simple, just make sure all three components of the conditional corpus pipeline are set to run with the right parameters and then click 'Run this Application' at the bottom of the window. Here's a screenshot:

![UNTSrecordsIE-app](img/UNTSrecordsIE-app.png "GATE UNTSrecordsIE app")


A sample UNTS treaty page annotated by the app (with annotations list and pop-up window) looks like this:

![POPconvAnnots](img/GATE-annots-POPconv.png "Annotated UNTS page of POP Convention")


A view of the Lucene Datastore Searcher with corpus statistics on the right and 'AdoptionPlace' chosen as the annotation type to search for:

![UNTSrecordsIE-Lucene-GATE](img/UNTSrecordsIE-Lucene-GATE.png "Screenshot of Lucene Datastore Searcher")


As for SPARQL queries, I use the Apache Jena ARQ command line tool, setting the JENA_HOME environment variable, and then running (for the UNTSmeta query):
```
sparql --data=$HOME/git/UNTS-treatyrecords-IE/examples/pilo_sampleUNTSdata.ttl --query=$HOME/git/UNTS-treatyrecords-IE/sparql/UNTSmeta.rq --results=CSV > ~/git/UNTS-treatyrecords-IE/examples/UNTSmetaSamples.csv
```
and likewise for the POPconv query:
```
sparql --data=$HOME/git/UNTS-treatyrecords-IE/examples/pilo_sampleUNTSdata.ttl --query=$HOME/git/UNTS-treatyrecords-IE/sparql/UNTSparticPOPconv.rq --results=CSV > ~/git/UNTS-treatyrecords-IE/examples/UNTSparticPOPconv.csv
```

Protégé has a SPARQL tool as well (activated under Window -> Tabs -> SPARQL Query), which does not require use of the command line. Simply open the `pilo_sampleUNTSdata.ttl` file in Protégé, then copy-paste the desired SPARQL query into it (from the query files in the 'sparql' folder of this directory) and then click 'Execute'.

The treaty participation data output by the query in `UNTSparticPOPconv.rq` is designed to be a suitable input to a D3 treaty participation map (like the ones published on https://globalAIgov.org) and does not distinguish between different forms of expressing consent to be bound (ratification, accession etc.). The columns are 'Participant', 'Signature', 'Consent' and 'EIF' (Entry into Force).


[^1]: Technically GATE only requires Java 8 or later, but there may have been changes in Java's regex engine between versions 8 and 11 (which was used for developing this application).

[^2]: If true, a mappingsURL would be required for BFO as GATE cannot load the ontology from http://purl.obolibrary.org/obo/bfo/2020/bfo.owl, presumably due to redirection
