Imports: {
import static gate.Utils.*;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
}

Phase: PopulateOntologyWithUNTSdata
Input: TreatyTitle AdoptionDate
Options: control = appelt

Rule: CreateInstancesAndProperties
({TreatyTitle}
{AdoptionDate}):inst
-->
{
  AnnotationSet instanceAnnots = inputAS.get();
  if (instanceAnnots == null || instanceAnnots.size() == 0) {
    System.err.println("Error: this document does not have any annotations!");
    return;
  }
  // generate treaty lookup map
  HashMap<String,String> URLmap = new HashMap<String,String>();
  String line = null;
  String dir = System.getProperty("user.home");
  String fn = dir + "/git/UNTS-treatyrecords-IE/examples/urlMap.tsv";
  try (BufferedReader csvReader = new BufferedReader(new FileReader(fn))) {
    while ((line = csvReader.readLine()) != null) {
      String[] arr = line.split("\t");
      URLmap.put(arr[0], arr[1]);
    }
  }
  catch (IOException err) {
    throw new JapeException(err);
  }
  // create a HashMap to look up instances by their className
  Map<OInstance,String> docInstMap = new HashMap<OInstance,String>();
  for (Annotation instanceAnn : instanceAnnots) {
    if (!instanceAnn.getType().matches("TreatyAction")) {
      String className = (String)instanceAnn.getFeatures()
        .get(gate.creole.ANNIEConstants.LOOKUP_CLASS_FEATURE_NAME);
      if (className != null) {
        OClass aClass = ontology.getOClass(ontology.createOURIForName(className));
        if (aClass == null) {
          System.err.println("Error class \""+ className +"\" does not exist! instanceAnn: " +
                             instanceAnn.getType());
          return;
        }
        String theInstanceText = gate.Utils.stringFor(doc, instanceAnn);
        OURI instanceURI = ontology.createOURIForName(className + "_" + theInstanceText.hashCode());
        instanceAnn.getFeatures().put("instanceURI", instanceURI.toString());
        if (!ontology.containsOInstance(instanceURI)) {
          OInstance inst = ontology.addOInstance(instanceURI, aClass);
          // add (truncated) label
          if (!className.matches("treatyPublication|treatySigAdRef|treatyDefSig|" +
                                 "treatyRatification|treatyAccession|treatySuccession")) {
            int StrLen = theInstanceText.length();
            if (StrLen > 100) {
              inst.setLabel(theInstanceText.substring(0,100), OConstants.ENGLISH);
            } else {
              inst.setLabel(theInstanceText.substring(0,StrLen), OConstants.ENGLISH);
            }
          }
          // add instance string (where appropriate)
          if (!className.matches("treatyPublication|location|treatyParticipant|treatySigAdRef|" +
                                 "treatyDefSig|treatyRatification|treatyAccession|treatySuccession")) {
            DatatypeProperty dataProp =
              ontology.getDatatypeProperty(ontology.createOURIForName("hasInstanceString"));
            try {
              inst.addDatatypePropertyValue(dataProp, new Literal(theInstanceText, OConstants.ENGLISH));
            }
            catch (InvalidValueException err) {
              throw new JapeException(err);
            }
          }
          docInstMap.put(inst, className);
        } else {
          OInstance inst = ontology.getOInstance(instanceURI);
          docInstMap.put(inst, className);
        }
      }// if (has class feature)
    }// if (filter)
  }// for each annotation in doc

  // generate hash for instance URIs
  String UNTSurl = stringFor(doc, instanceAnnots.get("UNTStreatyRecordURL"));
  String titlenad = URLmap.get(UNTSurl);
  String hash = "" + titlenad.hashCode();

  // prepare datetime formatters
  DateTimeFormatter textDate = DateTimeFormatter.ofPattern("d MMMM yyyy", Locale.ENGLISH);
  DateTimeFormatter tableDate = DateTimeFormatter.ofPattern("dd/MM/yyyy");

  // commonly used properties and instances
  ObjectProperty involves = ontology.getObjectProperty(ontology.createOURIForName("involves"));
  ObjectProperty hasParticipant = ontology.getObjectProperty(ontology.createOURIForName("hasParticipant"));
  DatatypeProperty dateDP = ontology.getDatatypeProperty(ontology.createOURIForName("hasDate"));

  String treatyTitleStr = stringFor(doc, instanceAnnots.get("TreatyTitle"));
  String treatyAdoptionDateStr = stringFor(doc, instanceAnnots.get("AdoptionDate"));
  LocalDate treatyAdoptionDate = LocalDate.parse(treatyAdoptionDateStr, tableDate);
  String xsdtreatyAdoptionDateStr = treatyAdoptionDate.format(DateTimeFormatter.ISO_LOCAL_DATE);

  OInstance UN = ontology.getOInstance(ontology.createOURIForName("UN"));
  OInstance UNTS = ontology.getOInstance(ontology.createOURIForName("UNTS"));

  // create OInstances that have no annotations
  // create or get treaty instance
  OInstance treatyInst = null;
  if (!docInstMap.containsValue("treaty")) {
    OClass treatyClass = ontology.getOClass(ontology.createOURIForName("treaty"));
    OURI treatyInstURI = ontology.createOURIForName("treaty_" + hash);
    if (!ontology.containsOInstance(treatyInstURI)) {
      treatyInst = ontology.addOInstance(treatyInstURI, treatyClass);
    } else {
      treatyInst = ontology.getOInstance(treatyInstURI);
    }
    docInstMap.put(treatyInst, "treaty");
  }

  // create treatyAdoption instance
  try {
    OClass treatyAdoptionClass = ontology.getOClass(ontology.createOURIForName("treatyAdoption"));
    OURI treatyAdoptionInstURI = ontology.createOURIForName("treatyAdoption_" + hash);
    OInstance treatyAdoptionInst = ontology.addOInstance(treatyAdoptionInstURI, treatyAdoptionClass);
    docInstMap.put(treatyAdoptionInst,"treatyAdoption");
    // add treatyAdoption properties
    treatyAdoptionInst.addObjectPropertyValue(involves, treatyInst);
    treatyAdoptionInst.addDatatypePropertyValue(dateDP, new Literal(xsdtreatyAdoptionDateStr,
                                                                    dateDP.getDataType()));
  }
  catch (InvalidValueException err) {
    throw new JapeException(err);
  }

  // create treatyEIF instance
  if (!instanceAnnots.get("TreatyEIFdate").isEmpty()) {
    try {
      OClass treatyEIFClass = ontology.getOClass(ontology.createOURIForName("generalTreatyEIF"));
      OURI treatyEIFInstURI = ontology.createOURIForName("generalTreatyEIF_" + hash);
      OInstance treatyEIFInst = ontology.addOInstance(treatyEIFInstURI, treatyEIFClass);
      docInstMap.put(treatyEIFInst, "generalTreatyEIF");
      // add treatyEIF properties
      treatyEIFInst.addObjectPropertyValue(involves, treatyInst);
      String dateStr = stringFor(doc, instanceAnnots.get("TreatyEIFdate"));
      LocalDate d = LocalDate.parse(dateStr, textDate);
      String xsdDateStr = d.format(DateTimeFormatter.ISO_LOCAL_DATE);
      treatyEIFInst.addDatatypePropertyValue(dateDP, new Literal(xsdDateStr,
                                                                 dateDP.getDataType()));
      // add nb of days elapsed since adoption
      DatatypeProperty daysDP = ontology.getDatatypeProperty(ontology.createOURIForName("incubDays"));
      Long daysdelta = ChronoUnit.DAYS.between(treatyAdoptionDate, d);
      treatyEIFInst.addDatatypePropertyValue(daysDP, new Literal(daysdelta.toString(),
                                                                 daysDP.getDataType()));
    }
    catch (InvalidValueException | DateTimeParseException err) {
      throw new JapeException(err);
    }
  }

  // create treatyRegistration instance
  if (!instanceAnnots.get("TreatyRegDate").isEmpty()) {
    try {
      String className = "treatyRegistration";
      OClass aClass = ontology.getOClass(ontology.createOURIForName(className));
      OURI instanceURI = ontology.createOURIForName(className + "_" + hash);
      OInstance inst = ontology.addOInstance(instanceURI, aClass);
      docInstMap.put(inst, className);
      // link registration instance to treaty instance and to UN
      inst.addObjectPropertyValue(involves, treatyInst);
      inst.addObjectPropertyValue(hasParticipant, UN);
      // add date
      String dateStr = stringFor(doc, instanceAnnots.get("TreatyRegDate"));
      LocalDate d = LocalDate.parse(dateStr, textDate);
      String xsdDateStr = d.format(DateTimeFormatter.ISO_LOCAL_DATE);
      inst.addDatatypePropertyValue(dateDP, new Literal(xsdDateStr, dateDP.getDataType()));
    }
    catch (InvalidValueException | DateTimeParseException err) {
      throw new JapeException(err);
    }
  }

  // automatically create properties/relations
  for (Annotation instanceAnn : instanceAnnots) {
    if (!instanceAnn.getType().matches("TreatyAction")) {
      String sourceClassName = (String)instanceAnn.getFeatures()
        .get(gate.creole.ANNIEConstants.LOOKUP_CLASS_FEATURE_NAME);
      if (sourceClassName != null) {
        OURI sourceInstOURI = ontology.createOURI((String)instanceAnn.getFeatures().get("instanceURI"));
        OInstance sourceInst = ontology.getOInstance(sourceInstOURI);
        // create object properties
        for (Map.Entry<Object, Object> e : instanceAnn.getFeatures().entrySet()) {
          if (!e.getKey().toString().matches("class|instanceURI|hasParticipant|lastRetrieved|" +
                                             "hasDate|hasNotifDate|hasEffectDate|hasURL")) {
            String objPropName = e.getKey().toString();
            ObjectProperty objProp = ontology.getObjectProperty(ontology.createOURIForName(objPropName));      
            String targetInstClassName = e.getValue().toString();
            OInstance targetInst = null;
            // number of keys for this value should be 1
            if (docInstMap.containsValue(targetInstClassName)) {
              for (Map.Entry<OInstance,String> en : docInstMap.entrySet()) {
                if (en.getValue().equals(targetInstClassName)) {targetInst = en.getKey();}
              }
              try {
                sourceInst.addObjectPropertyValue(objProp, targetInst);
              }
              catch (InvalidValueException | NullPointerException err) {
                throw new JapeException(err);
              }
            }
          }
          // create treaty participant instances
          if (e.getKey().toString().matches("hasParticipant")) {
            try {
              String objPropName = e.getKey().toString();
              ObjectProperty objProp = ontology.getObjectProperty(ontology.createOURIForName(objPropName));
              String targetInstName = e.getValue().toString();
              OURI targetInstURI = ontology.createOURIForName("treatyParticipant_" + targetInstName.hashCode());
              OInstance targetInst = ontology.getOInstance(targetInstURI);
              sourceInst.addObjectPropertyValue(objProp, targetInst);
            }
            catch (InvalidValueException | NullPointerException err) {
              throw new JapeException(err);
            }
          }
          // add data properties for dates and datetimes
          if (e.getKey().toString().matches("lastRetrieved|hasDate|hasNotifDate|hasEffectDate")) {
            String dataPropName = e.getKey().toString();
            DatatypeProperty dataProp = ontology.getDatatypeProperty(ontology.createOURIForName(dataPropName));
            String dateStr = e.getValue().toString();
            try {
              if (dataPropName.matches("lastRetrieved")) {
                String xsdDateTimeStr = dateStr;
                sourceInst.addDatatypePropertyValue(dataProp, new Literal(xsdDateTimeStr,
                                                                          dataProp.getDataType()));
              } else {
                LocalDate d = LocalDate.parse(dateStr, tableDate);
                String xsdDateStr = d.format(DateTimeFormatter.ISO_LOCAL_DATE);
                sourceInst.addDatatypePropertyValue(dataProp, new Literal(xsdDateStr,
                                                                          dataProp.getDataType()));
              }
            }
            catch (InvalidValueException | DateTimeParseException err) {
              throw new JapeException(err);
            }
          }
          // add data property for URLs
          if (e.getKey().toString().matches("hasURL")) {
            String dataPropName = e.getKey().toString();
            DatatypeProperty dataProp = ontology.getDatatypeProperty(ontology.createOURIForName(dataPropName));
            String dataPropStr = e.getValue().toString();
            try {
              sourceInst.addDatatypePropertyValue(dataProp, new Literal(dataPropStr));
            }
            catch (InvalidValueException err) {
              throw new JapeException(err);
            }
          }
        }//for Map.Entry
      }// if has class feature
    }//if relevant instanceAnn
  }//for instanceAnn

  // create UNTSvolume instance (at the end to avoid trouble with isPartOf treatyPublication properties)
  if (!instanceAnnots.get("UNTSvolRef").isEmpty()) {
    try {
      String className = "treatyPublication";
      OClass aClass = ontology.getOClass(ontology.createOURIForName(className));
      String instStr = stringFor(doc, instanceAnnots.get("UNTSvolNb"));
      OURI instanceURI = ontology.createOURIForName("UNTSvol" + instStr);
      OInstance inst = ontology.addOInstance(instanceURI, aClass);
      inst.setLabel("UNTS volume " + instStr, OConstants.ENGLISH);
      docInstMap.put(inst, className);
      // link it to UNTS
      ObjectProperty partOf = ontology.getObjectProperty(ontology.createOURIForName("isPartOf"));
      inst.addObjectPropertyValue(partOf, UNTS);
      // add UNTSvolume pdf version URL
      if (!instanceAnnots.get("UNTSvolURL").isEmpty()) {
        String url = stringFor(doc, instanceAnnots.get("UNTSvolURL"));
        DatatypeProperty dataProp = ontology.getDatatypeProperty(ontology.createOURIForName("hasURL"));
        inst.addDatatypePropertyValue(dataProp, new Literal(url));
      }
    }
    catch (InvalidValueException err) {
      throw new JapeException(err);
    }
  }
  // System.out.println("[DONE] ["+ treatyTitleStr +"] ["+ Instant.now().toString() +"]");
}
